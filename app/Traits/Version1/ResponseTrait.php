<?php namespace App\Traits\Version1;

trait ResponseTrait
{
    private $code;
    private $message;
    private $results;

    private function setParameter($code, $message, $results)
    {
        $this->code = $code;
        $this->message = $message;
        $this->results = $results;
    }

    private function getParameterArray()
    {
        $result = [
            'error_code' => $this->code,
            'messages'   => $this->message,
            'results'    => $this->results
        ];
        return $result;
    }

    private function jsonResponse()
    {
        return response()->json($this->getParameterArray(), 200);
    }

    public function response($code, $message, $results)
    {
        $this->setParameter($code, $message, $results);
        return $this->jsonResponse();
    }

    public function responseError()
    {
        $this->setParameter(404, 'Error', []);
        return $this->jsonResponse();
    }

    public function validationErrors(array $errors)
    {
        //$errors
        if (!key($errors) == 0) {
            $errors = array_column($errors, 0);
        }
        $this->setParameter(422, $errors, []);
        return $this->jsonResponse();
    }
}