<?php namespace App\Traits;

/**
 * Class RequestResponseErrors
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Traits
 */
trait RequestResponseErrors
{
    public function jsonResponse(array $errors)
    {

        $errors = array_column($errors, 0);
        $response = [
            'messages' => $errors,
            'error_code'     => 422
        ];

        return response()->json($response, 200);
    }
}