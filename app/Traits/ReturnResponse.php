<?php namespace App\Traits;


trait ReturnResponse
{
    protected $err_msg;

    protected $http_status;

    /**
     * set HTTP status
     * @param string $err_msg
     * @param int $err_code
     */
    protected function setHttpStatus($err_msg,$err_code)
    {
        $this->err_msg = $err_msg;
        $this->http_status = $err_code;
    }

    /**
     * set success HTTP status
     */
    protected function setSuccessHttpStatus()
    {
        $this->err_msg = 'Successfully!';
        $this->http_status = 200;
    }

    /**
     * set server error HTTP status
     */
    protected function setServerErrorHttpStatus()
    {
        $this->err_msg = 'Server is busy!';
        $this->http_status = 503;
    }


    /**
     * Public method get error messages
     * @return array
     */
    public function getErrorMessages()
    {
        return (array)$this->err_msg;
    }

    /**
     * Public method get http status
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->http_status;
    }
}