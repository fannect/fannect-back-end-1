<?php namespace App\Traits;

use Route;

trait RequestTrait
{
    public function isCurrentActionName($action)
    {
        $route = Route::currentRouteAction();
        $match = [];
        preg_match('/@(.*)/', $route, $match);
        if ($match[1] == $action) {
            return true;
        }

        return false;
    }

    public function getCurrentActionName()
    {
        $route = Route::currentRouteAction();
        $match = [];
        preg_match('/@(.*)/', $route, $match);
        return $match[1];
    }
}