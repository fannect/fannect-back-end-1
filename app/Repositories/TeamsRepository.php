<?php namespace App\Repositories;

use App\Modules\Users\User;
use DB;
use App\Modules\Teams\Team;
use App\Traits\ReturnResponse;

/**
 * Class TeamsRepository
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Repositories
 */
class TeamsRepository
{

    use ReturnResponse;

    protected $teams;

    protected $user;

    /**
     * public
     * @return mixed
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * return all teams data
     *
     * @internal param int $skip
     * @internal param int $limit
     */
    public function getAllTeams()
    {
        $this->teams = Team::all();
        $this->setSuccessHttpStatus();
    }

    /**
     * search teams team's name
     * @param $teams
     */
    public function search($teams)
    {
        extract($teams);
        isset($skip) ?: $skip = 0;
        isset($limit) ?: $limit = 10;
        $this->teams = Team::where('full_name', 'LIKE', '%'.$team_name.'%')
            ->skip($skip)->limit($limit)->get();
        $this->setSuccessHttpStatus();
    }

    /**
     * @param array $user_info
     * @return bool
     */
    public function userTeams(array $user_info){
        extract($user_info);
        DB::beginTransaction();
        try{
            $teams = explode(",",$teams);
            $this->user = User::find($user_id);
            $this->user->teams()->sync($teams);
            DB::commit();
            return true;
        }catch(\Exception $e){
            DB::rollBack();
            return false;
        }
    }

    /**
     * @param array $user_info
     *
     * @return bool
     */
    public function getUserFavoriteTeams(array $user_info){
        extract($user_info);
        $this->user = User::find($user_id);
        $this->teams = $this->user->teams;
        return true;
    }

}