<?php namespace App\Repositories;

use App\Modules\Api\Curl;
use App\Modules\Api\News;
use App\Modules\Posts\Post;
use DB;

/**
 * Class NewsRepository
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Repositories
 */
class NewsRepository
{

    const LEAD_ARTICLES = 'http://bleacherreport.com/api/front/lead_articles.json?page=1&perpage=21';

    const ARTICLE = 'http://api.bleacherreport.com/api/v1/articles/';

    protected $_articles;

    protected $_news;

    protected $_post;

    protected $_curl;

    function __construct(Curl $curl)
    {
        $this->_curl = $curl;
    }

    protected function newNews(array $article)
    {
        $this->_news = new News($article);
    }

    public function getLastedNews()
    {
        $this->_curl->get(self::LEAD_ARTICLES);
        $result = json_decode($this->_curl->response, true);

        array_map([$this, 'getNewsInfo'], $result);
    }

    protected function insertConditional($params)
    {
        $result;
        $params != 'none' ? $result = true : $result = false;

        return $result;
    }

    protected function getNewsInfo($news)
    {
        $url = self::ARTICLE.intval($news['permalink']);
        $this->_curl->get($url);
        $result = $this->_curl->response;
        $result = json_decode($result, true);
        if ($this->insertConditional($result['article']['pageData']['team'])) {
//            $this->_articles[] = $result;
            $article = [
                'br_id'     => $result['article']['id'],
                'title'     => $result['article']['title'],
                'image_url' => $result['article']['primaryPhoto']['url'],
                'content'   => DB::getPdo()->quote($result['article']['publicBody'])
            ];
            $this->insertNews($article);
        }
    }

    protected function insertNews(array $news)
    {
        $this->newNews($news);
        $this->_post = Post::create(['user_id' => 1]);
        $this->_post->news()->save($this->_news);
    }
}