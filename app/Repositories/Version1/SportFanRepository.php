<?php namespace App\Repositories\Version1;

use App\Models\Version1\SocialNetwork;
use App\Models\Version1\SportFan;
use App\Models\Version1\Token;
use App\Traits\Version1\ResponseTrait;
use Illuminate\Support\Str;
use DB;

class SportFanRepository extends SportFan
{
    use ResponseTrait;

    /**
     * Check SN is connected
     *
     * @param $social_network_uid
     *
     * @return bool (true: Connected,false: Not yet connect)
     */
    private function socialNetworkIsConnected($social_network_uid)
    {
        $result = SocialNetwork::firstByAttributes(compact('social_network_uid'));
        if (is_null($result)) {
            return true;
        }
        return false;
    }

    /**
     * Register fannect
     *
     * @param $request
     * @param $request ->is_facebook 1: Facebook, 2: Twitter, 3: Fannect
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fannectRegister($request)
    {
        DB::beginTransaction();
        try {
            if ($request['is_facebook'] != 3) {
                if (!$this->socialNetworkIsConnected($request['social_network_uid'])) {     //Social Network UID Existed
                    $socialNetworkUID = SocialNetwork::firstByAttributes(
                        ['social_network_uid' => $request['social_network_uid']]
                    );
                    $fan = $this->find($socialNetworkUID->id);
                    if ($fan->is_facebook == $request['is_facebook']) {
                        $fan->tokens()->create($request);
                    } else {
                        return $this->validationErrors(200, ['Can\'t login to this account'], []);
                    }
                } else {
                    $fan = $this->create($request);
                    $fan->socialNetworks()->create($request);
                }
            } else {
                $fan = $this->create($request);
                $fan->tokens()->create($request);
            }
            DB::commit();
            return $this->getSportFan($fan->id);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->responseError();
        }
    }

    public function getSportFan($id)
    {
        $user = $this->with('tokens', 'socialNetworks')->find($id);
        if (is_null($user)) {
            return $this->responseError();
        } else {
            $relations = array_keys($user->relationsToArray());
            foreach ($relations as $relation) {
                $relation = Str::camel($relation);
                $user->$relation->map(
                    function ($relation) {
                        $relation->setHidden(['id', 'user_id', 'created_at', 'updated_at']);
                        return $relation;
                    }
                );
            }
            return $this->response(200, 'Success', $user);
        }
    }

    public function loginSportFan(array $request)
    {
        $fan = $this->firstByAttributes(['email' => $request['email']]);
        $auth = \Hash::check($request['password'], $fan->password);
        if ($auth) {
            $logged = Token::firstByAttributes(['device_uid' => $request['device_uid']]);
            if (is_null($logged)) {
                $fan->tokens()->create($request);
            }
            return $this->getSportFan($fan->id);
        } else {
            return $this->responseError();
        }
    }
}