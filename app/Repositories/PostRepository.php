<?php namespace App\Repositories;

use App\Modules\Posts\Comment;
use App\Modules\Posts\Content;
use App\Modules\Posts\Image;
use App\Modules\Posts\Post;
use App\Modules\Users\Friend;
use App\Modules\Users\Token;
use App\Modules\Users\User;
use App\Modules\Users\UserRepository;
use App\Traits\ReturnResponse;
use DB;
use App\Traits\PushNotificationTrait;

/**
 * Class PostRepository
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Repositories
 */
class PostRepository
{
    use ReturnResponse,PushNotificationTrait;

    protected $post;

    protected $user;

    protected $images;

    protected $content;

    protected $comments;

    /**
     * set Images
     *
     * @param array $images
     */
    public function setImages(array $images)
    {
        //$images = json_encode($images);
        //foreach ($images as $image) {
        //    $this->images[] = new Image(['url' => $image]);
        //}
        $this->images = new Image(['url'=>$images]);
    }

    /**
     * set Content
     *
     * @param array $content
     */
    public function setContent(array $content)
    {
        $this->content = new Content($content);
    }

    /**
     * set comment
     *
     * @param array $comments
     */
    public function setComments(array $comments)
    {
        $this->comments = new Comment($comments);
    }

    /**
     * return post
     *
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    public function getPostInfoById($post_id = null)
    {
        is_null($post_id) ? $id = $this->post->id : $id = $post_id;
        $result = Post::with('comments', 'images', 'contents', 'boos')
            ->whereId($id)->first();
        $this->setSuccessHttpStatus();
        return $result;
    }

    /**
     * new post
     *
     * @param array $post_info
     *
     * @return bool
     */
    public function newPost(array $post_info)
    {
        extract($post_info);
        DB::beginTransaction();
        try {
            $this->post = Post::create($post_info);
            if (isset($images)) {
                $this->setImages($images);
                $this->post->images()->save($this->images);
            }
            if (isset($content)) {
                $this->setContent(compact('content'));
                $this->post->contents()->save($this->content);
            }

            DB::commit();
            $this->setSuccessHttpStatus();
            $followerDeviceUIDs = $this->getFollowerDeviceUID($user_id);
            $messages =
                [
                    'badge'        => 1,
                    'type'         => 'new_post',
                    'results'      => [
                        'post_id'  => $this->post->id
                    ]
                ];
            foreach($followerDeviceUIDs as $followerDeviceUID){
                $this->pushNotification($messages,$followerDeviceUID);
            }
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * new comment
     *
     * @param array $comment_info
     *
     * @return bool
     */
    public function newComment(array $comment_info)
    {
        extract($comment_info);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $this->user->comments()
                ->attach([$post_id => ['content' => $content]]);
            $this->setSuccessHttpStatus();
            DB::commit();

            return true;
        } catch (\Exception $e) {
            $this->setServerErrorHttpStatus();
            DB::rollBack();

            return false;
        }
    }

    /**
     * boo a post
     *
     * @param array $boo_info
     *
     * @return bool
     */
    public function actionBoo(array $boo_info)
    {
        extract($boo_info);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $this->user->boos()->attach([$post_id]);
            $this->setSuccessHttpStatus();
            DB::commit();

            return true;
        } catch (\Exception $e) {
            $this->setServerErrorHttpStatus();
            DB::rollBack();

            return false;
        }
    }


    /**
     * cheer a post
     *
     * @param array $cheer_info
     *
     * @return bool
     */
    public function actionCheer(array $cheer_info)
    {
        extract($cheer_info);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $this->user->cheers()->attach([$post_id]);
            $this->setSuccessHttpStatus();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            $this->setServerErrorHttpStatus();
            DB::rollBack();
            return false;
        }
    }

    private function getFollowerDeviceUID($id)
    {
        $follower = array_column(Friend::where('following_id', $id)->get()->toArray(), 'follower_id');
        $followerDeviceUID = array_column(Token::whereIn('user_id', $follower)->get()->toArray(), 'device_uid');
        return $followerDeviceUID;
    }
}