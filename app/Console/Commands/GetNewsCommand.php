<?php namespace App\Console\Commands;

use App\Modules\Users\User;
use Illuminate\Console\Command;
use App\Repositories\NewsRepository;

class GetNewsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:news';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

    protected $_news_repo;

    /**
     * Create a new command instance.
     *
     * @param NewsRepository $news_repo
     */
	public function __construct(NewsRepository $news_repo)
	{
		parent::__construct();
        $this->_news_repo = $news_repo;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->_news_repo->getLastedNews();
        $this->info('Update complelte!');
	}
}
