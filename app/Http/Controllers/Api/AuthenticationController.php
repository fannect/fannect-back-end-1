<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Users\UserRepository;
use Illuminate\Http\Request;
use Response;

/**
 * Class AuthenticationController
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Controllers\Api
 */
class AuthenticationController extends Controller
{

    /**
     * @var UserRepository
     */
    private $user_repo;

    function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo;
    }

    /**
     * Authentication Method
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAuthentication(Request $request)
    {
        extract($request);
//        if ($type == 'socialNetwork') {
//            $this->user_repo->socialNetworkAuthentication($email);
//        } else {
//            $result = $this->user_repo->emailAuthentication($email, $password);
//            if ($result === false) {
//                return Response::json(['error_msg' => 'Authentication Failed!'], 401);
//            } else {
//                return Response::json(['error_msg' => 'Authentication Successfully', 'resource' => $result], 200);
//            }
//        }
        $result  = $this->user_repo->attempt($request);
        if(!$result){
            return Response::json(['error_msg' => 'Authentication Failed!'], 401);
        } else {
            return Response::json(['error_msg' => 'Authentication Successfully', 'resource' => $result], 200);
        }
    }

    /**
     * Logout Method
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postLogout(){
        $this->user_repo->logout();
        return Response::json(['error_msg' => 'Logout Successfully'],200);
    }

}
