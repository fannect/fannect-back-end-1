<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\FollowTeamsRequest;
use App\Http\Requests\Users\ListFollowTeamsRequest;
use App\Http\Requests\Users\SocialNetworkRequest;
use App\Http\Requests\Users\UserInfomationRequest;
use App\Modules\Users\Chat;
use App\Modules\Users\FriendRepository;
use App\Modules\Users\User;
use App\Modules\Users\UserRepository;
use App\Http\Requests\Users\UserRequest;
use App\Http\Requests\Users\LoginRequest;
use App\Modules\Users\UserSocialNetwork;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Users\RegisterViaSocialNetworkRequest;
use App\Http\Requests\Users\LogoutRequest;
use App\Http\Requests\Users\ConnectSocialNetworkRequest;
use App\Http\Requests\Users\ListFanBySocialNetworkId;
use App\Http\Requests\Users\ForgotPasswordRequest;
use App\Http\Requests\Users\UpdateUserSettingsRequest;

/**
 * Class UserController
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Controllers\Api
 */
class UserController extends Controller
{

    /**
     * @var UserRepository
     */
    private $user_repo;

    /**
     * @var UserRepository
     */
    private $friend_repo;

    /**
     * Dependency Injection User Repository
     *
     * @param UserRepository   $user_repo
     * @param FriendRepository $friend_repo
     */
    public function __construct(
        UserRepository $user_repo,
        FriendRepository $friend_repo
    ) {
        $this->user_repo = $user_repo;
        $this->friend_repo = $friend_repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        return 'Hello, This is User API';
    }

    /**
     * Register method
     *
     * @param UserRequest $request
     *
     * @return Response
     */
    public function postRegister(UserRequest $request)
    {
        $result = $this->user_repo->register($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * /**
     * Register via social network methodLoginViaSocialNetworkRequest
     *
     * @param RegisterViaSocialNetworkRequest $request
     *
     * @return mixed
     */
    public function postSocialNetworkAuth(
        RegisterViaSocialNetworkRequest $request
    ) {
        $result = $this->user_repo->socialNetworkAuth($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * Update method
     *
     * @param UserRequest $request
     *
     * @return Response
     */
    public function putUpdate(UserRequest $request)
    {
        $result = $this->user_repo->update($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * Forgot password User
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function postForgotPassword(ForgotPasswordRequest $request)
    {
        $result = $this->user_repo->forgot($request->get('email'));

        return $this->getRepoResponse($result, false);
    }

    /**
     * Login method
     *
     * @param LoginRequest $request
     *
     * @return mixed
     */
    public function postLogin(LoginRequest $request)
    {
        $result = $this->user_repo->login($request->all());

        return $this->getRepoResponse($result);
    }

    //    /**
    //     * Login via social networks
    //     * @param LoginViaSocialNetworkRequest $request
    //     * @return mixed
    //     */
    //    public function postLoginViaSocialNetwork(LoginViaSocialNetworkRequest $request)
    //    {
    //        $result = $this->user_repo->loginViaSocialNetwork($request->all());
    //
    //        return $this->getRepoResponse($result);
    //    }

    /**
     * Logout method
     *
     * @param LogoutRequest $request
     *
     * @return mixed
     */
    public function deleteLogout(LogoutRequest $request)
    {
        $result = $this->user_repo->logout($request->all());

        return $this->getRepoResponse($result, false);
    }

    /**
     * @param UserInfomationRequest $request
     *
     * @return mixed
     */
    public function postUserInfomation(UserInfomationRequest $request)
    {
        $result = $this->user_repo->setUser($request->get('user'));

        return $this->getRepoResponse($result);
    }

    /**
     * connect to social network
     *
     * @param ConnectSocialNetworkRequest $request
     *
     * @return mixed
     */
    public function postConnectSocialNetwork(
        ConnectSocialNetworkRequest $request
    ) {
        $result = $this->user_repo->connectSocialNetwork($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * list fan by social network Uid
     *
     * @param ListFanBySocialNetworkId $request
     *
     * @return mixed
     */
    public function postListSocialNetworkId(ListFanBySocialNetworkId $request)
    {
        $list = $request->get('social_network_uid');
        if (!is_array($list)) {
            $list = explode(',', $list);
        }

        $this->friend_repo->listFanBySocialNetworkId($list);
        $response = [
            'messages'    => $this->friend_repo->getErrorMessages(),
            'error_code'  => $this->friend_repo->getHttpStatus(),
            'friend_list' => $this->friend_repo->getListFan()
        ];

        return response()->json($response);
    }

    /**
     * set user setting
     *
     * @param UpdateUserSettingsRequest $request
     *
     * @return mixed
     */
    public function putSettings(UpdateUserSettingsRequest $request)
    {
        $result = $this->user_repo->setUserSetting($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * @param FollowTeamsRequest $request
     *
     * @return mixed
     */
    public function putFollowTeams(FollowTeamsRequest $request)
    {
        $result = $this->user_repo->followTeams($request->all());

        return $this->getRepoResponse($result);
    }

    /**
     * @param ListFollowTeamsRequest $request
     *
     * @return mixed
     */
    public function postListFollowTeams(ListFollowTeamsRequest $request)
    {
        $result
            = $this->user_repo->listUserFollowTeams($request->get('user_id'));

        return $this->getRepoResponse($result);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postChatMsg(Request $request)
    {
        $msgs = Chat::where('user_to_id', $request->get('user_id'))
            ->where('received', 0)->get()->toArray();
        $return_arr = [];
        foreach ($msgs as $msg) {

            $msg['from'] = $msg['user_from_id'];
            $msg['to'] = $msg['user_to_id'];
            $msg['msg'] = $msg['message'];
            unset($msg['user_from_id']);
            unset($msg['user_to_id']);
            unset($msg['message']);
            $return_arr[] = $msg;
        }

        return response()->json($return_arr, 200);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function postSearch(Request $request)
    {
        $user = $request->get('user_info');
        $result = User::where('email', $user)
            ->orWhere('first_name', 'LIKE', '%'.$user.'%')
            ->orWhere('last_name', 'LIKE', '%'.$user.'%')->get()->toArray();
        return response()->json($result, 200);
    }

    /**
     * Return repository user infomation
     *
     * @param      $result
     * @param bool $uinfo
     *
     * @return mixed
     */
    private function getRepoResponse($result, $uinfo = true)
    {

        is_null($this->user_repo->getCustomErrorCode()) ?
            $error_code = $this->user_repo->getHttpStatus()
            : $error_code = $this->user_repo->getCustomErrorCode();

        $response = [
            'error_code' => $error_code,
            'messages'   => $this->user_repo->getErrorMessages()
        ];

        if ($result && $uinfo) {

            $user_info = $this->user_repo->getUser();

            if (!array_key_exists('social_networks', $user_info)) {
                if (!empty($this->user_repo->getUserSocialNetwork())) {
                    $user_info = array_add($user_info, 'social_networks',
                        $this->user_repo->getUserSocialNetwork());
                }
            }

            if (!array_key_exists('tokens', $user_info)) {
                if (!empty($this->user_repo->getUserToken())) {
                    $user_info = array_add($user_info, 'tokens',
                        $this->user_repo->getUserToken()->toArray());
                }
            }

            if (!array_key_exists('user_settings', $user_info)) {
                $user_info = array_add($user_info, 'user_settings',
                    $this->user_repo->getUserSetting());
            }

            $user_info = $user_info->toArray();

            $user_info = array_map([$this, 'replaceNullArrayValues'],
                $user_info);

            $response = array_add($response, 'results', $user_info);
        }

        return response()->json($response, $this->user_repo->getHttpStatus());
    }

    public function deleteDisconnectSocialNetwork(SocialNetworkRequest $request) {
        UserSocialNetwork::where('user_id', $request->get('user_id'))
            ->where('social_network_uid', $request->get('social_network_uid'))
            ->delete();
        $response = [
            'error_code' => '200',
            'messages'   => 'OK!'
        ];

        return response()->json($response);
    }

    private function replaceNullArrayValues($value)
    {
        if (is_null($value)) {
            return "";
        } else {
            return $value;
        }
    }
}