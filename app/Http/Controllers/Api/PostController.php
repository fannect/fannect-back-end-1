<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Posts\GetPostInfoRequest;
use App\Http\Requests\Posts\MotivateRequest;
use App\Repositories\PostRepository;
use App\Http\Requests\Posts\NewPostRequest;
use App\Http\Requests\Posts\NewCommentRequest;

/**
 * Class PostController
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Controllers\Api
 */
class PostController extends Controller
{

    protected $post_repo;

    function __construct(PostRepository $post_repo)
    {
        $this->post_repo = $post_repo;
    }

    /**
     * new post
     * @param NewPostRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postNewPost(NewPostRequest $request)
    {
        $result = $this->post_repo->newPost($request->all());
        return $this->getRepoResponse($result);
    }

    /**
     * new comment
     * @param NewCommentRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postNewComment(NewCommentRequest $request)
    {
        $result = $this->post_repo->newComment($request->all());

        return $this->getRepoResponse($result, false);
    }

    /**
     * get post info
     * @param GetPostInfoRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postGetPostInfo(GetPostInfoRequest $request)
    {
        $result = $this->post_repo->getPostInfoById($request->get('post_id'));
        $response = [
            'error_code' => $this->post_repo->getHttpStatus(),
            'messages'   => $this->post_repo->getErrorMessages(),
            'results'    => $result
        ];

        return response()->json($response, 200);
    }

    /**
     * boo a post
     * @param MotivateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postBooAPost(MotivateRequest $request){
        $result = $this->post_repo->actionBoo($request->all());
        return $this->getRepoResponse($result,false);
    }

    /**
     * cheer a post
     * @param MotivateRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postCheerAPost(MotivateRequest $request){
        $result = $this->post_repo->actionCheer($request->all());
        return $this->getRepoResponse($result,false);
    }

    /**
     * return repository response
     * @param $result
     * @param bool $post
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRepoResponse($result, $post = true)
    {
        $response = [
            'error_code' => $this->post_repo->getHttpStatus(),
            'messages'   => $this->post_repo->getErrorMessages()
        ];
        if ($result && $post) {
            $response = array_add($response, 'results', $this->post_repo->getPostInfoById());
        }

        return response()->json($response, 200);
    }
}