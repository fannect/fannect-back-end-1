<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Friends\ListFollowerRequest;
use Illuminate\Http\Request;
use App\Modules\Users\FriendRepository;
use App\Http\Requests\Friends\FollowUserRequest;
use App\Http\Requests\Friends\UnFollowUserRequest;

/**
 * Class FriendController
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Controllers\Api
 */
class FriendController extends Controller
{

    protected $friend_repo;

    function __construct(FriendRepository $friend_repo)
    {
        $this->friend_repo = $friend_repo;
    }

    /**
     * @param FollowUserRequest $request
     *
     * @return mixed
     */
    public function postFollow(FollowUserRequest $request)
    {
        $this->friend_repo->followUser($request->all());

        return $this->getRepoResponse(false);
    }

    /**
     * @param ListFollowerRequest $request
     *
     * @return mixed
     */
    public function postListFollower(ListFollowerRequest $request)
    {
        $this->friend_repo->getFollower($request->get('user_id'));

        return $this->getRepoResponse();
    }

    /**
     * @param ListFollowerRequest $request
     *
     * @return mixed
     */
    public function postListFollowing(ListFollowerRequest $request)
    {
        $this->friend_repo->getFollowing($request->get('user_id'));

        return $this->getRepoResponse();
    }

    /**
     * @param UnFollowUserRequest $request
     *
     * @return mixed
     */
    public function deleteUnFollow(UnFollowUserRequest $request)
    {
        $this->friend_repo->unFollow($request->all());

        return $this->getRepoResponse(false);
    }

    /**
     * @param bool $listfan
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function getRepoResponse($listfan = true)
    {
        $response = [
            'error_code' => $this->friend_repo->getHttpStatus(),
            'messages'   => $this->friend_repo->getErrorMessages()
        ];

        if ($listfan) {
            $response = array_add($response, 'results',
                $this->friend_repo->getListFan());
        }

        return response()->json($response, $response['error_code']);
    }

}
