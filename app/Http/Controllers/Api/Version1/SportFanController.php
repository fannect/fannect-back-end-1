<?php namespace App\Http\Controllers\Api\Version1;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Version1\SportFanRequest;
use App\Repositories\Version1\SportFanRepository;
use App\Traits\Version1\ResponseTrait;

class SportFanController extends Controller
{
    use ResponseTrait;
    private $sport;

    function __construct(SportFanRepository $sport)
    {
        $this->sport = $sport;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(SportFanRequest $request)
    {
        return $this->sport->fannectRegister($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        return $this->sport->getSportFan($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(SportFanRequest $request, $id)
    {
        return 'TRUE';
    }

    public function login(SportFanRequest $request)
    {
        return $this->sport->loginSportFan($request->all());
    }

}
