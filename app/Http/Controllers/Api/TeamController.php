<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\Teams\GetUserFavoriteTeamsRequest;
use App\Http\Requests\Teams\SearchRequest;
use App\Http\Requests\Teams\UserTeamsRequest;
use App\Repositories\TeamsRepository;
use Illuminate\Http\Request;

/**
 * Class TeamController
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Controllers\Api
 */
class TeamController extends Controller
{

    protected $teamsRepo;

    function __construct(TeamsRepository $teams_repo)
    {
        $this->teamsRepo = $teams_repo;
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAll(Request $request)
    {
        $this->teamsRepo->getAllTeams();

        return $this->getRepoResponse(true);
    }

    /**
     * @param SearchRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postSearch(SearchRequest $request)
    {
        $this->teamsRepo->search($request->all());

        return $this->getRepoResponse(true);
    }

    /**
     * @param UserTeamsRequest $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function putUserTeam(UserTeamsRequest $request)
    {
        $result = $this->teamsRepo->userTeams($request->all());

        return $this->getRepoResponse($result, false);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postGetUserFavoriteTeams(GetUserFavoriteTeamsRequest $request)
    {
        $this->teamsRepo->getUserFavoriteTeams($request->all());
        return $this->getRepoResponse(true);
    }

    /**
     * @param      $result
     * @param bool $info
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function getRepoResponse($result, $info = true)
    {
        $response = [
            'error_code' => $this->teamsRepo->getHttpStatus(),
            'messages'   => $this->teamsRepo->getErrorMessages()
        ];
        if ($result && $info) {
            $response = array_add($response, 'results',
                $this->teamsRepo->getTeams());
        }

        return response()->json($response, 200);
    }
}
