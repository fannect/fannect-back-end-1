<?php
Route::get('/', 'WelcomeController@index');

Route::group(
    ['prefix' => 'api'],
    function () {
        Route::group(
            ['prefix' => 'user'],
            function () {
                Route::controller('/', 'Api\UserController');
            }
        );
        Route::group(
            ['prefix' => 'friend'],
            function () {
                Route::controller('/', 'Api\FriendController');
            }
        );
        Route::group(
            ['prefix' => 'post'],
            function () {
                Route::controller('/', 'Api\PostController');
            }
        );
        Route::group(
            ['prefix' => 'team'],
            function () {
                Route::controller('/', 'Api\TeamController');
            }
        );
    }
);

/**
 * API Version 1
 */
Route::group(
    ['namespace' => 'Api\Version1'],
    function () {
        Route::group(
            ['prefix' => 'api/v1'],
            function () {
                //Sport Fan
                Route::resource('sport-fan', 'SportFanController', ['only' => ['store', 'show', 'update']]);
                Route::post('sport-fan/login', 'SportFanController@login');
            }
        );
    }
);
Route::get('/test',function(){
    return view('test');
});