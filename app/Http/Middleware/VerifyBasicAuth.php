<?php namespace App\Http\Middleware;

use Closure;
use App\Modules\Users\Token;

class VerifyBasicAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->server('PHP_AUTH_USER') && $request->server('PHP_AUTH_PW')) {$auth = [
                'user_id' => $request->server('PHP_AUTH_USER'),
                'token'   => $request->server('PHP_AUTH_PW')
            ];
            $result = Token::findMany($auth);
            if (empty($result->toArray())) {
                return response()->make(['message'=>'Authentication fails','code'=>401], 401);
            }
            return $next($request);
        }


        $headers = [
            'WWW-Authenticate' => 'Basic realm="API AUTHENTICATION REQUIRED!"'
        ];
        $error_msg = [
            'message' => 'Authentication required',
            'code'    => 401
        ];

        return response()->make($error_msg, 401, $headers);
    }
}
