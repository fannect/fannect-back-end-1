<?php namespace App\Http\Middleware;

use Closure;

/**
 * Class CheckRawRequest
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Middleware
 */
class CheckRawRequest
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Replace raw data
        $raw = $request->instance()->getContent();
        if ($raw) {
            $raw = json_decode($raw, true);
            if ($raw) {
                $request->replace($raw);
            }
        }

        return $next($request);
    }

}