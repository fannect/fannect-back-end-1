<?php namespace App\Http\Requests\Posts;

use App\Http\Requests\Request;

/**
 * Class NewPostRequest
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Posts
 */
class NewPostRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'   => 'required|exists:users,id',
            'content'   => 'required_without:images',
            'images'    => 'required_without:content',
            'longitude' => 'required',
            'latitude'  => 'required'
        ];
    }

    /**
     * Return error msgs if validation fails
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $errors = array_column($errors, 0);
        $response = [
            'messages' => $errors,
            'error_code'     => 422
        ];

        return response()->json($response, 200);
    }

}
