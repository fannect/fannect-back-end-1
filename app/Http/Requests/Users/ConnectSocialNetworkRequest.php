<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

/**
 * Class ConnectSocialNetworkRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class ConnectSocialNetworkRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'            => 'required|exists:users,id',
            'social_network_uid' => 'required|unique:user_social_networks',
            'is_facebook'        => 'required|boolean'
        ];
    }

    /**
     * Return error msgs if validation fails
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $errors = array_column($errors, 0);
        $response = [
            'messages' => $errors,
            'error_code'     => 422
        ];

        return response()->json($response, 200);
    }

}
