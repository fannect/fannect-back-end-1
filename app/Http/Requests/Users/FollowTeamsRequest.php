<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;
use App\Traits\RequestResponseErrors;

/**
 * Class FollowTeamsRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class FollowTeamsRequest extends Request
{

    use RequestResponseErrors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'teams'   => 'required'
        ];
    }

    /**
     * Return error msgs if validation fails
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->jsonResponse($errors);
    }

}
