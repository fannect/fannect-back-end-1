<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

/**
 * Class LoginRequest
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class LoginRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'      => 'required|email|exists:users',
            'password'   => 'required|between:6,255',
            'device_uid' => 'required'
        ];
    }

    /**
     * Custom Error messages
     * @return array
     */
    public function messages()
    {
        return [
            'email.exists' => 'email.exists'
        ];
    }

    /**
     * Return error messages when validate fail
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $error_code = 422;

        //Return error_code to client handle error messages
        if (array_key_exists('email', $errors)) {
            foreach ($errors['email'] as $value) {
                if ($value === 'email.exists') {
                    $error_code = 4001;
                }
            }
        }

        $errors = array_column($errors, 0);
        $response = [
            'error_code' => $error_code,
            'messages'   => $errors
        ];

        return response()->json($response, 200);
    }

}
