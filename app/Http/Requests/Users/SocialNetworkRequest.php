<?php namespace App\Http\Requests\Users;

use App\Traits\RequestTrait;
use App\Http\Requests\Request;
use App\Traits\RequestResponseErrors;

/**
 * Class SocialNetworkRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class SocialNetworkRequest extends Request
{
    use RequestTrait;
    use RequestResponseErrors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isCurrentActionName('deleteDisconnectSocialNetwork')) {
            return [
                'social_network_uid' => 'required|exists:user_social_networks',
                'user_id'            => 'required|exists:user_social_networks',
            ];
        }
    }

    /**
     * return errors messages
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->jsonResponse($errors);
    }

}
