<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

/**
 * Class LogoutRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class LogoutRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_uid' => 'required',
            'user_id'    => 'required',
            'token'      => 'required'
        ];
    }

    /**
     * Return error msgs if validation fails
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $errors = array_column($errors, 0);
        $response = [
            'messages' => $errors,
            'error_code'     => 422
        ];

        return response()->json($response, 200);
    }
}
