<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;

/**
 * Class UserRequest
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class UserRequest extends Request
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('post')) {
            $rule = [
                'email'      => 'required|min:3|email|unique:users',
                'password'   => 'required|between:6,255|confirmed',
                'device_uid' => 'required'
            ];
        } elseif ($this->isMethod('put') || $this->isMethod('patch')) {
            $rule = [
                'user_id' => 'required'
            ];
            if (!is_null($this->get('password'))) {
                $rule['password'] = 'required|confirmed';
            }
        }

        return $rule;
    }

    /**
     * Custom Error messages
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique' => 'email.unique'
        ];
    }

    /**
     * Return error messages when validate fail
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $error_code = 422;

        //Return error_code to client handle error messages
        if (array_key_exists('email', $errors)) {
            foreach ($errors['email'] as $value) {
                if ($value === 'email.unique') {
                    $error_code = 4000;
                }
            }
        }

        $errors = array_column($errors, 0);
        $response = [
            'error_code' => $error_code,
            'messages'   => $errors
        ];

        return response()->json($response, 200);
    }

}
