<?php namespace App\Http\Requests\Users;

use App\Http\Requests\Request;


/**
 * Class RegisterViaSocialNetworkRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Users
 */
class RegisterViaSocialNetworkRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        return [
//            'email'              => 'required|min:3|email|unique:users',
//            'social_network_uid' => 'required|unique:user_social_networks',
//            'is_facebook'        => 'required|boolean',
//            'device_uid'         => 'required',
//            'first_name'         => 'required',
//            'last_name'          => 'required',
//            'gender'             => 'required'
//        ];
        return [
            'social_network_uid' => 'required',
            'is_facebook'        => 'required|boolean',
            'device_uid'         => 'required'
        ];
    }

    /**
     * Custom Error messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.unique' => 'email.unique'
        ];
    }

    /**
     * Return error messages when validate fail
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        $error_code = 422;

        //Return error_code to client handle error messages
        if (array_key_exists('email', $errors)) {
            foreach ($errors['email'] as $value) {
                if ($value === 'email.unique') {
                    $error_code = 4000;
                }
            }
        }

        $errors = array_column($errors, 0);
        $response = [
            'error_code' => $error_code,
            'messages'   => $errors
        ];

        return response()->json($response, 200);
    }
}
