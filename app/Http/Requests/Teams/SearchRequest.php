<?php namespace App\Http\Requests\Teams;

use App\Http\Requests\Request;
use App\Traits\RequestResponseErrors;

/**
 * Class SearchRequest
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Http\Requests\Teams
 */
class SearchRequest extends Request
{

    use RequestResponseErrors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_name' => 'required'
        ];
    }

    /**
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->jsonResponse($errors);
    }
}
