<?php namespace App\Http\Requests\Teams;

use App\Http\Requests\Request;
use App\Traits\RequestResponseErrors;

class UserTeamsRequest extends Request
{

    use RequestResponseErrors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'teams'   => 'required'
        ];
    }

    /**
     * Return error
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->jsonResponse($errors);
    }

}
