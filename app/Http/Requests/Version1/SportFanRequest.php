<?php namespace App\Http\Requests\Version1;

use App\Http\Requests\Request;
use App\Traits\Version1\ResponseTrait;
use App\Traits\RequestTrait;

class SportFanRequest extends Request
{
    use ResponseTrait, RequestTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $action = $this->getCurrentActionName();
        if ($action == 'store') {
            return [
                'email'              => 'required|email|unique:users',
                'password'           => 'required_if:is_facebook,3|between:6,255|confirmed',
                'social_network_uid' => 'required_if:is_facebook,1|required_if:is_facebook,2|unique:user_social_networks',
                'device_uid'         => 'required',
                'is_facebook'        => 'required'
            ];
        } elseif ($action == 'update') {
            return [
                'email'    => 'email|unique:users,email,'.$this->segment(4),
                'password' => 'between:6,255|confirmed'
            ];
        } elseif ($action == 'login') {
            return [
                'email'       => 'required|email|exists:users',
                'password'    => 'required|between:6,255',
                'device_uid'  => 'required',
                'is_facebook' => 'integer|size:3'
            ];
        }
        return false;
    }

    /**
     * Return error msgs if validation fails
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return $this->validationErrors($errors);
    }

}