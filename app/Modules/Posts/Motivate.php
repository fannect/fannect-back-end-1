<?php namespace App\Modules\Posts;

use Eloquent;

/**
 * Class Motivate
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Posts
 */
class Motivate extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_id',
        'is_boo'
    ];

    /**
     * define post relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo('App\Modules\Posts\Post');
    }

    /**
     * define user relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Modules\Users\User');
    }
}