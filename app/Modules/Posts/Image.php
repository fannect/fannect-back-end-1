<?php namespace App\Modules\Posts;

use Eloquent;

/**
 * Class Image
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Posts
 */
class Image extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'url'
    ];

    protected $casts = [
            'post_id'   => 'string',
            'url'       => 'array'
        ];

    /**
     * define post relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo('App\Modules\Posts\Post');
    }
}