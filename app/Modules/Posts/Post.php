<?php namespace App\Modules\Posts;

use Eloquent;

/**
 * Class Post
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Posts
 */
class Post extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'user_id',
            'share_id',
            'longitude',
            'latitude'
        ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts
        = [
            'user_id'   => 'string',
            'share_id'  => 'string',
            'longitude' => 'string',
            'latitude'  => 'string',
        ];

    /**
     * define user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('App\Modules\Users\User');
    }

    /**
     * define images relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasOne('App\Modules\Posts\Image');
    }

    /**
     * define comments relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->belongsToMany('App\Modules\Users\User', 'comments')
            ->withPivot('content')->withTimestamps();
    }

    /**
     * define content relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function contents()
    {
        return $this->hasOne('App\Modules\Posts\Content');
    }

    /**
     * define boo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boos()
    {
        return $this->belongsToMany('App\Modules\Users\User', 'user_unlike');
    }

    /**
     * define cheer relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cheers()
    {
        return $this->belongsToMany('App\Modules\Users\User', 'user_like');
    }

    /**
     * define news relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function news()
    {
        return $this->hasOne('App\Modules\Api\News');
    }
}