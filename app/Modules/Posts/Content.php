<?php namespace App\Modules\Posts;

use Eloquent;

/**
 * Class Content
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Posts
 */
class Content extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_contents';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'content'
    ];

    /**
     * define post relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->hasOne('App\Modules\Posts\Content');
    }
}