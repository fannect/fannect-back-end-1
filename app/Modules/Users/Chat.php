<?php namespace App\Modules\Users;
use Eloquent;

/**
 * App\Modules\Users\Chat
 *
 */
class Chat extends Eloquent {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_chat';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_from_id',
        'user_to_id',
        'message',
        'received'
    ];
}