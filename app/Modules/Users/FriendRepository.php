<?php namespace App\Modules\Users;

use App\Modules\Users\UserRepository;
use League\Flysystem\Exception;
use PushNotification;
use App\Traits\PushNotificationTrait;

/**
 * Class FriendRepository
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 */
class FriendRepository
{
    use PushNotificationTrait;
    /**
     * error messages
     *
     * @var
     */
    protected $err_msg;

    /**
     * http status code
     *
     * @var
     */
    protected $http_status;

    /**
     * list fan
     *
     * @var
     */
    protected $list_fan;

    /**
     * @var UserRepository
     */
    protected $user_repo;

    /**
     * constructor
     *
     * @param UserRepository $user_repo
     */
    function __construct(UserRepository $user_repo)
    {
        $this->user_repo = $user_repo;
    }


    /**
     * Set Http status method
     *
     * @param string $err_msg
     * @param int    $err_code
     */
    protected function setHttpStatus(
        $err_msg = 'Successfully!',
        $err_code = 200
    ) {
        $this->err_msg = $err_msg;
        $this->http_status = $err_code;
    }

    /**
     * Public method get user infomation
     *
     * @return mixed
     */
    public function getListFan()
    {
        return $this->list_fan;
    }

    /**
     * Public method get error messages
     *
     * @return array
     */
    public function getErrorMessages()
    {
        return (array) $this->err_msg;
    }

    /**
     * Public method get http status
     *
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->http_status;
    }

    /**
     * list fan by social network Id
     *
     * @param array $list_uid
     */
    public function listFanBySocialNetworkId(array $list_uid)
    {
        $list = UserSocialNetwork::whereIn('social_network_uid', $list_uid)
            ->get();
        $list = array_column($list->toArray(), 'user_id');
        $list = array_unique($list);
        $this->getFanInfoById($list);
    }

    /**
     * find fan infomation by Id
     *
     * @param array $list
     */
    protected function getFanInfoById(array $list)
    {
        $list_fan = User::whereIn('id', $list)->orderBy('id', 'asc')->get();
        $this->setHttpStatus();
        $this->list_fan = $list_fan;
    }

    /**
     * @param array $user
     *
     * @return \Exception
     */
    public function followUser(array $user)
    {
        extract($user);
        $duplicate = Friend::where('follower_id', $follower_id)
            ->where('following_id', $following_id)->first();
        if (is_null($duplicate)) {

            Friend::create($user);

            $follower = User::find($follower_id);
            $following = User::find($following_id);
            $this->setHttpStatus();

            $messages =
                [
                    'badge'        => 1,
                    'actionLocKey' => $following->first_name.' '.$following->last_name.' had followed you.',
                    'body'         => $following->first_name.' '.$following->last_name.' had followed you.',
                    'type'         => 'follow',
                    'results'      => [
                        'follower'  => $follower->toArray(),
                        'following' => $following->toArray()
                    ]
                ];

            try {
                $devices = array_column($following->tokens->toArray(), 'device_uid');
                foreach ($devices as $device) {
                    $this->pushNotification($messages, $device);
                }
            } catch (\Exception $e) {
                return $e;
            }
        } else {
            $this->setHttpStatus('Duplicated follow', 422);
        }
    }

    /**
     * get Follower
     *
     * @param $id
     */
    public function getFollower($id)
    {
        $friends = Friend::where('following_id', $id)->get()->toArray();
        $friends = array_column($friends, 'follower_id');

        $friends = User::whereIn('id', $friends)->get()->toArray();
        $this->list_fan = $friends;
        $this->setHttpStatus();
    }

    /**
     * get Following
     *
     * @param $id
     */
    public function getFollowing($id)
    {
        $friends = Friend::where('follower_id', $id)->get()->toArray();
        $friends = array_column($friends, 'following_id');

        $friends = User::whereIn('id', $friends)->get()->toArray();
        $this->list_fan = $friends;
        $this->setHttpStatus();
    }

    /**
     * un follow user
     *
     * @param array $user
     *
     * @throws \Exception
     */
    public function unFollow(array $user)
    {
        extract($user);
        Friend::where('follower_id', $follower_id)
            ->where('following_id', $following_id)->delete();
        $this->setHttpStatus();
        $follower = User::find($follower_id);
        $following = User::find($following_id);
        $messages = [
            'type'    => 'unfollow',
            'results' => [
                'follower'  => $follower->toArray(),
                'following' => $following->toArray()
            ]
        ];
        //        try {
        //            $devices = array_column($following->tokens->toArray(),
        //                'device_uid');
        //            $push = \PushNotification::app('Fannect');
        //            foreach ($devices as $device) {
        //                $push->to($device)->send(json_encode($messages));
        //            }
        //        } catch (\Exception $e) {
        //            return $e;
        //        }
    }
}