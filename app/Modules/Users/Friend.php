<?php namespace App\Modules\Users;

use Carbon\Carbon;
use Eloquent;
use Hash;

/**
 * Class User
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 */
class Friend extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'friends';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'follower_id',
        'following_id'
    ];

    protected $casts
        = [
            'follower_id'   => 'string',
            'following_id' => 'string'
        ];
}