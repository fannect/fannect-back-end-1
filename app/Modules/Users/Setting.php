<?php namespace App\Modules\Users;

use Eloquent;

/**
 * App\Modules\Users\Setting
 *
 * @property-read \Modules\Users\User $users 
 */
class Setting extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_setting';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'user_id',
            'challenge',
            'chat',
            'friend'
        ];

    protected $casts
        = [
            'user_id'   => 'string',
            'challenge' => 'string',
            'chat'      => 'string',
            'friend'    => 'string',
        ];

    /**
     * User relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('Modules\Users\User');
    }

}
