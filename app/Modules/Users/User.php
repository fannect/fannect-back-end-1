<?php namespace App\Modules\Users;

use Carbon\Carbon;
use Eloquent;
use Hash;

/**
 * Class User
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 * @property-write mixed $password 
 * @property-write mixed $birth_day 
 * @property-write mixed $is_facebook 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Users\Token[] $tokens 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Users\UserSocialNetwork[] $socialNetworks 
 * @property-read \App\Modules\Users\Setting $settings 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Posts\Post[] $posts 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Posts\Post[] $comments 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Posts\Post[] $boos 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Posts\Post[] $cheers 
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Modules\Teams\Team[] $teams
 */
class User extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'email',
            'password',
            'is_facebook',
            'first_name',
            'last_name',
            'gender',
            'birth_day',
            'phone_number',
            'profile_image_url',
            'profile_cover_url',
            'about'
        ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts
        = [
            'email'             => 'string',
            'password'          => 'string',
            'is_facebook'       => 'string',
            'first_name'        => 'string',
            'last_name'         => 'string',
            'gender'            => 'string',
            'birth_day'         => 'string',
            'phone_number'      => 'string',
            'profile_image_url' => 'string',
            'profile_cover_url' => 'string',
            'about'             => 'string'
        ];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'is_facebook'];

    /**
     * Hash password
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    /**
     * Format date time
     *
     * @param $value
     */
    public function setBirthDayAttribute($value)
    {
        $this->attributes['birth_day'] = Carbon::parse($value)
            ->toDateTimeString();
    }

    /**
     * Set is facebook attribute
     *
     * @param $value
     */
    public function setIsFacebookAttribute($value)
    {
        $this->attributes['is_facebook'] = boolval($value);
    }

    /**
     * Token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tokens()
    {
        return $this->hasMany('\App\Modules\Users\Token');
    }

    /**
     * User Social Network relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialNetworks()
    {
        return $this->hasMany('\App\Modules\Users\UserSocialNetwork');
    }

    /**
     * User Setting relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function settings()
    {
        return $this->hasOne('App\Modules\Users\Setting');
    }

    /**
     * define posts relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany('App\Modules\Posts\Post');
    }

    /**
     * define user comment post relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function comments()
    {
        return $this->belongsToMany('App\Modules\Posts\Post', 'comments')
            ->withPivot('content')->withTimestamps();
    }

    /**
     * define boo relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boos()
    {
        return $this->belongsToMany('App\Modules\Posts\Post', 'user_unlike');
    }

    /**
     * define cheer relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cheers()
    {
        return $this->belongsToMany('App\Modules\Posts\Post', 'user_like');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany('App\Modules\Teams\Team');
    }
}