<?php namespace App\Modules\Users;

use App\Modules\Users\User;
use League\Flysystem\Exception;
use App\Traits\ReturnResponse;
use DB;
use Mail;
use Hash;

/**
 * Class UserRepository
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 */
class UserRepository
{

    use ReturnResponse;

    /**
     * user infomation
     *
     * @var
     */
    protected $user;

    /**
     * remember token
     *
     * @var
     */
    protected $token;

    /**
     * social network
     *
     * @var
     */
    protected $social_network;

    /**
     * custom errors code
     *
     * @var
     */
    protected $custom_error_code;

    /**
     * user settings
     *
     * @var
     */
    protected $user_settings;

    /**
     * user favorite teams
     *
     * @var
     */
    protected $user_favorite_teams;

    /**
     * Public method get custom error code
     *
     * @return mixed
     */
    public function getCustomErrorCode()
    {
        return $this->custom_error_code;
    }

    /**
     * Public method get user's favorite teams
     *
     * @return mixed
     */
    public function getFavoriteTeams()
    {
        return $this->user_favorite_teams;
    }

    /**
     * Public method get user infomation
     *
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Public method get error messages
     *
     * @return array
     */
    public function getErrorMessages()
    {
        return (array)$this->err_msg;
    }

    /**
     * Public method get http status
     *
     * @return mixed
     */
    public function getHttpStatus()
    {
        return $this->http_status;
    }

    /**
     * Public method get user token
     *
     * @return mixed
     */
    public function getUserToken()
    {
        return $this->token;
    }

    /**
     * Public method get user socialnetwork
     *
     * @return mixed
     */
    public function getUserSocialNetwork()
    {
        return $this->social_network;
    }

    /**
     * Public method get user settings
     *
     * @return mixed
     */
    public function getUserSetting()
    {
        return $this->user_settings;
    }

    /**
     * Set remember token
     *
     * @param $device_uid
     */
    protected function setToken($device_uid)
    {
        $token = str_random(40);
        $this->token = new Token([
            'token'      => $token,
            'device_uid' => $device_uid
        ]);
    }

    /**
     * Set settings options
     * Set settings options
     *
     * @param array $user_info
     */
    protected function setSettings(array $user_info)
    {
        $this->user_settings = new Setting($user_info);
    }

    /**
     * set social network
     *
     * @param array $user_info
     */
    protected function setSocialNetwork(array $user_info)
    {
        $this->social_network = new UserSocialNetwork($user_info);
    }

    /**
     * Set User settings
     */
    protected function setUserInfomation()
    {
        $this->social_network = $this->user->socialNetworks;
        $this->token = $this->user->tokens;
        $this->user_settings = $this->user->settings;
    }

    /**
     * Register method
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function register(array $user_info)
    {
        DB::beginTransaction();
        try {
            extract($user_info);
            $user_setting = ['challenge' => 1, 'chat' => 1, 'friend' => 1];
            $this->setToken($device_uid);
            $this->setSettings($user_setting);

            $this->user = User::create($user_info);
            $this->user->tokens()->save($this->token);
            $this->user->settings()->save($this->user_settings);

            DB::commit();
            $this->setSuccessHttpStatus();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * Update method
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function update(array $user_info)
    {
        extract($user_info);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $this->user->update($user_info);
            DB::commit();
            $this->setSuccessHttpStatus();
            $this->setUserInfomation();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * Forgot password by email
     *
     * @param $email
     *
     * @return bool
     */
    public function forgot($email)
    {
        $this->user = User::where('email', $email)->first();
        if (is_null($this->user)) {
            $this->setHttpStatus('This email does not exist', 200);

            return false;
        } else {
            DB::beginTransaction();
            try {
                $password = str_random();
                $this->user->update(compact('password'));
                $user = $this->user;
                //Send email
                Mail::send('mails.forgotpassword', compact('password', 'user'),
                    function ($message) use ($user) {
                        $message->to($user->email,
                            $user->first_name." ".$user->last_name)
                            ->subject('Forgot password!');
                    });
                DB::commit();
                $this->setSuccessHttpStatus();

                return true;
            } catch (Exception $e) {
                DB::rollBack();
                $this->setServerErrorHttpStatus();

                return false;
            }
        }
    }

    /**
     * Login method
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function login(array $user_info)
    {
        extract($user_info);
        $this->user = User::where('email', $email)->first();
        if (Hash::check($password, $this->user->password)) {
            //password match, generate token & return
            DB::beginTransaction();
            try {
                $has_token = Token::where('device_uid', $device_uid)->first();
                if ($has_token) {
                    $this->token = $has_token;
                } else {
                    $this->setToken($device_uid);
                    $this->user->tokens()->save($this->token);
                }
                DB::commit();
                $this->social_network = $this->user->socialNetworks;
                $this->user_settings = $this->user->settings;
                $this->setSuccessHttpStatus();
                return true;
            } catch (Exception $e) {
                DB::rollBack();
                $this->setServerErrorHttpStatus();
                return false;
            }
        } else {
            //Password doesn't match
            $this->setHttpStatus('Your email and password does not match!', 4001);
            return false;
        }
    }

    /**
     * register Via Social Network
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function registerViaSocialNetwork(array $user_info)
    {
        extract($user_info);
        DB::beginTransaction();
        try {
            $this->user = User::create($user_info);
            $this->setSocialNetwork($user_info);
            $this->setToken($device_uid);
            $this->user->socialNetworks()->save($this->social_network);
            $this->user->tokens()->save($this->token);
            $this->user->registered = "0";
            $user_setting = ['challenge' => 1, 'chat' => 1, 'friend' => 1];
            $this->setSettings($user_setting);
            $this->user->settings()->save($this->user_settings);
            DB::commit();
            $this->setSuccessHttpStatus();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * check register or login when authorized from social network
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function socialNetworkAuth(array $user_info)
    {
        extract($user_info);

        $this->social_network = UserSocialNetwork::where('social_network_uid', $social_network_uid)->where('is_facebook', $is_facebook)->first();
        if (is_null($this->social_network)) {
            return $this->registerViaSocialNetwork($user_info);
        } else {
            return $this->loginViaSocialNetwork($user_info);
        }
    }

    /**
     * Login via social networks
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function loginViaSocialNetwork(array $user_info)
    {
        extract($user_info);
        $this->social_network = UserSocialNetwork::where('social_network_uid', $social_network_uid)->where('is_facebook', $is_facebook)->first();
        if (is_null($this->social_network)) {
            $this->setHttpStatus('Unauthorized', 4001);
        } else {
            $this->user = User::find($this->social_network->user_id);
            if (!is_null($this->user->is_facebook) && boolval($this->user->is_facebook) !== boolval($is_facebook)) {
                $this->setHttpStatus('Can not using this account to login', 4001);
                return false;
            } else {
                $this->user->registered = "1"; //Flag variable set to front endgi
                $this->setToken($device_uid);
                $this->user->tokens()->save($this->token);
                $this->user_settings = $this->user->settings;
                //$this->connectSocialNetwork($user_info);
                $this->setSuccessHttpStatus();
                return true;
            }
        }
    }

    /**
     * Logout (Delete token where token,uid,device_id)
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function logout(array $user_info)
    {
        extract($user_info);

        DB::beginTransaction();
        try {
            Token::where('user_id', $user_id)->where('token', $token)
                ->where('device_uid', $device_uid)->delete();
            DB::commit();
            $this->setSuccessHttpStatus();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * Return userInfomation
     *
     * @param $user
     *
     * @return bool
     */
    public function setUser($user)
    {
        $this->user = User::where('id', $user)->orWhere('email', $user)
            ->first();
        if (is_null($this->user)) {
            $social = UserSocialNetwork::where('social_network_uid', $user)
                ->first();
            if (is_null($social)) {
                $this->setHttpStatus('This user does not exist!', 4001);

                return false;
            }
            $this->user = User::find($social->user_id);
        }

        $this->setUserInfomation();

        $this->setSuccessHttpStatus();

        return true;
    }

    /**
     * connect social network
     *
     * @param array $user_info
     *
     * @return bool
     */
    public function connectSocialNetwork(array $user_info)
    {
        DB::beginTransaction();
        try {
            extract($user_info);
            $this->user = User::find($user_id);
            $social = new UserSocialNetwork($user_info);
            $this->user->socialNetworks()->save($social);
            $this->social_network = $social;
            $this->setSuccessHttpStatus();
            DB::commit();

            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * update user setting
     *
     * @param $user_settings
     *
     * @return bool
     */
    public function setUserSetting($user_settings)
    {
        extract($user_settings);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $this->user->settings()->update($user_settings);
            DB::commit();
            $this->setUserInfomation();
            $this->setSuccessHttpStatus();

            return true;
        } catch (Exception $e) {
            DB::rollBack();
            $this->setServerErrorHttpStatus();

            return false;
        }
    }

    /**
     * follow teams
     *
     * @param array $info
     *
     * @return bool
     */
    public function followTeams(array $info)
    {
        extract($info);
        DB::beginTransaction();
        try {
            $this->user = User::find($user_id);
            $list_team = explode(',', $teams);
            $this->user->teams()->sync($list_team);
            $this->setSuccessHttpStatus();
            DB::commit();

            return true;
        } catch (\Exception $e) {
            $this->setServerErrorHttpStatus();
            DB::rollBack();

            return false;
        }
    }

    /**
     * List user's favorite team
     *
     * @param $user_id
     *
     * @return bool
     */
    public function listUserFollowTeams($user_id)
    {
        $this->user = User::find($user_id);
        $this->user_favorite_teams = $this->user->teams;

        return true;
    }
}