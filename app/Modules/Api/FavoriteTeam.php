<?php namespace App\Modules\Api;

/**
 * Class FavoriteTeam
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Api
 */
class FavoriteTeam {

    private $api_link = '';

    private $curl;

    function __construct(Curl $curl)
    {
        $this->curl = $curl;
    }


    public function getDataFromApi(){
        $result = $this->curl->get($this->api_link,[]);
        //PROCESS WITH $result
    }
}