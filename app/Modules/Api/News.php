<?php namespace App\Modules\Api;

use Eloquent;

class News extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'post_id',
            'br_id',
            'title',
            'image_url',
            'content'
        ];

    /**
     * define post relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo('App\Modules\Posts\Post');
    }
}