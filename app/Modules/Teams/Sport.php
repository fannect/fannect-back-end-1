<?php namespace App\Modules\Teams;

use Eloquent;

class Team extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'full_name'
        ];

    public function users(){
        return $this->belongsToMany('App\Modules\Users\User');
    }
}
