<?php namespace App\Modules\Emails;

use Illuminate\Support\Facades\Mail;

/**
 * Class EmailProvider
 *
 * @author Huy Huỳnh-Viết-Quang
 * @package App\Modules\Emails
 */
class EmailProvider {

    private $forgot_passwd_tpl = 'mails.forgotpassword';

    /**
     * Send email reset password
     * @param array $data
     * @param $mailto
     */
    public static function resetPassword(array $data){
        //data are content parse to view email
        //mailto is colosure parameter for send method below
        Mail::send('mails.forgotpassword',$data,function($message) {
//            $message->from('forgot@xxx.com','FORGOT PASS NAME');
//            $message->to($data->email);
            dd($data);
        });
    }
}