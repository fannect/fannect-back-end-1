<?php
require('../../vendor/autoload.php');

$server = new Hoa\Websocket\Server(
    new Hoa\Socket\Server('tcp://0.0.0.0:9947')
//    new Hoa\Socket\Server('tcp://localhost:9947')
);

//Manages the message event to get send data for each client using the broadcast method
$server->on('message', function (Hoa\Core\Event\Bucket $bucket) {
    $data = $bucket->getData();
    $data_arr = json_decode($data['message'], true);

    print_r($data_arr);

    if ($data_arr['type'] == 'read') {
        readMessage($data_arr['id']);
    } elseif ($data_arr['type'] == 'received') {
        receivedMessage($data_arr['id']);
    }
    else {
        //Insert message to database
        $id = insertMsg($data_arr['msg'], $data_arr['from'], $data_arr['to']);
        //Add id to array for response
        $data_arr['id'] = $id;
        $data_arr['type'] = 'unread';
    }

    $data_arr = json_encode($data_arr);
    //Response to client
    $bucket->getSource()->broadcast($data_arr);
    $bucket->getSource()->send($data_arr);

    return;
});
//Execute the server
$server->run();

function connectDB()
{
    $connect = new PDO('mysql:host=localhost;dbname=fannect', 'root',
        '1qazxsw2');

    return $connect;
}

function insertMsg($message, $from, $to)
{
    $connect = connectDB();
    $stmt = $connect
        ->prepare("INSERT INTO user_chat(`user_from_id`, `user_to_id`,`message`,`created_at`,`updated_at`) VALUES(?, ?, ?,NOW(),NOW())");
    $stmt->execute([$from, $to, $message]);

    return $connect->lastInsertId();
}

function returnHistory($from, $to)
{
    $connect = connectDB();
    $stmt
        = $connect->prepare("SELECT * FROM `user_chat` WHERE `user_from_id` = :from AND `user_to_id` = :to OR `user_from_id` = :to AND `user_to_id` = :from");
    $stmt->execute([':from' => $from, ':to' => $to]);
    $value = $stmt->fetch();

    return json_encode($value);
}

function readMessage($msg_id)
{
    $connect = connectDB();
    $stmt
        = $connect->prepare("UPDATE `user_chat` SET `read`=1 WHERE id=:msg_id");
    $stmt->execute([':msg_id' => $msg_id]);
}

function receivedMessage($msg_id)
{
    $connect = connectDB();
    $stmt
        = $connect->prepare("UPDATE `user_chat` SET `received`=1 WHERE id=:msg_id");
    $stmt->execute([':msg_id' => $msg_id]);
}