<?php namespace App\Models\Version1;

use Eloquent;
use Illuminate\Support\Str;

/**
 * Class User
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 * @property-read \App\Modules\Users\User $users
 */
class Token extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'token', 'device_uid'];

    protected $casts = [
        'user_id'    => 'string',
        'token'      => 'string',
        'device_uid' => 'string'
    ];

    protected $hidden = [];

    public static function boot()
    {
        parent::boot();
        static::creating(
            function ($token) {
                $token->token = Str::random(40);
            }
        );
    }

    /**
     * User relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo('\App\Modules\Users\User');
    }

}