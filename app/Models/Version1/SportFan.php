<?php namespace App\Models\Version1;

use Carbon\Carbon;
use Hash;
use Eloquent;

/**
 * Class User
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 */
class SportFan extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'is_facebook',
        'first_name',
        'last_name',
        'gender',
        'birth_day',
        'phone_number',
        'profile_image_url',
        'profile_cover_url',
        'about'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email'             => 'string',
        'password'          => 'string',
        'is_facebook'       => 'string',
        'first_name'        => 'string',
        'last_name'         => 'string',
        'gender'            => 'string',
        'birth_day'         => 'string',
        'phone_number'      => 'string',
        'profile_image_url' => 'string',
        'profile_cover_url' => 'string',
        'about'             => 'string'
    ];

    /**
     * Hash password
     *
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['password'] = Hash::make($value);
        }
    }

    /**
     * Format date time
     *
     * @param $value
     */
    public function setBirthDayAttribute($value)
    {
        $this->attributes['birth_day'] = Carbon::parse($value)->toDateTimeString();
    }

    /**
     * Set is facebook attribute
     *
     * @param $value
     */
    public function setIsFacebookAttribute($value)
    {
        $this->attributes['is_facebook'] = intval($value);
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tokens()
    {
        return $this->hasMany('\App\Models\Version1\Token', 'user_id', 'id');
    }

    public function socialNetworks()
    {
        return $this->hasMany('\App\Models\Version1\SocialNetwork', 'user_id', 'id');
    }
}