<?php namespace App\Models\Version1;

use Eloquent;

/**
 * Class UserSocialNetwork
 *
 * @author  Huy Huỳnh-Viết-Quang
 * @package App\Modules\Users
 * @property-read \App\Modules\Users\User $users
 * @property-write mixed                  $is_facebook
 */
class SocialNetwork extends Eloquent
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_social_networks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'social_network_uid', 'is_facebook'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id'            => 'string',
        'social_network_uid' => 'string',
        'is_facebook'        => 'string'
    ];

    public function sportFans()
    {
        return $this->belongsTo('\App\Models\Version1\SportFan');
    }

}