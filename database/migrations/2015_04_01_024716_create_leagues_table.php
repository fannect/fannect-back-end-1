<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaguesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('leagues', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('sport_id')->unsigned()->index();
            $table->foreign('sport_id')->references('id')->on('sport_names')->onDelete('cascade');
            $table->string('league_name');
            $table->string('league_key');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('leagues');
	}

}
