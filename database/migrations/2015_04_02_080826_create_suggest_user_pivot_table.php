<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestUserPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_guest_score', function(Blueprint $table)
		{
			$table->integer('match_id')->unsigned()->index();
			$table->foreign('match_id')->references('id')->on('schedules')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('home_score');
            $table->integer('away_score');
            $table->boolean('is_win');
            $table->integer('get_points');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_guest_score');
	}

}
