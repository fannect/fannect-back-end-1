<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedules', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('event_key');
            $table->timestamp('game_time');
            $table->integer('home')->unsigned()->index();
            $table->integer('away')->unsigned()->index();
            $table->integer('stadium_id')->unsigned()->index();
            $table->foreign('stadium_id')->references('id')->on('stadiums')->onDelete('cascade');
            $table->foreign('home')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('away')->references('id')->on('teams')->onDelete('cascade');
            $table->string('coverage');
            $table->integer('home_score');
            $table->integer('away_score');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedules');
	}

}
