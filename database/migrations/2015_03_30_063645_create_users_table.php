<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->boolean('is_facebook')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->boolean('gender');
            $table->string('profile_image_url');
            $table->string('profile_cover_url');
            $table->timestamp('birth_day');
            $table->text('about');
            $table->string('phone_number');
            $table->integer('invited_by')->unsigned()->index();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
