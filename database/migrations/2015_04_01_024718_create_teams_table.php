<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teams', function(Blueprint $table)
		{
            $table->increments('id');
//            $table->string('conference_key');
//            $table->string('conference_name');
            $table->string('full_name');
            $table->string('location_name')->nullable();
//            $table->boolean('needs_processing');
//            $table->integer('league_id')->unsigned()->index();
//            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade');
//            $table->integer('sport_id')->unsigned()->index();
//            $table->foreign('sport_id')->references('id')->on('sport_names')->onDelete('cascade');
//            $table->integer('stadium_id')->unsigned()->index();
//            $table->foreign('stadium_id')->references('id')->on('stadiums')->onDelete('cascade');
//            $table->string('team_key');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teams');
	}

}
