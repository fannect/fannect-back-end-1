<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStadiumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stadiums', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('coords');
            $table->integer('location');
            $table->integer('name');
            $table->string('stadium_key');
            $table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stadiums');
	}

}
