<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserChatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_chat', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_from_id')->unsigned()->index();
            $table->foreign('user_from_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_to_id')->unsigned()->index();
            $table->foreign('user_to_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('message');
            $table->boolean('read')->default(0);
            $table->boolean('received')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_chat');
	}

}
